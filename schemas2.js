/*
 * Schema class Definition
 */

function Schemas(data) {
    this.initData(data);
    this.checkProperty();
    this.initTools();
    this.initHtml();
    this.initEvents();

    this.y = window.scrollY + this.draw.node.getBoundingClientRect().top;
    this.y = window.scrollX + this.draw.node.getBoundingClientRect().left;
};

Schemas.prototype.tools = new Map();

Schemas.prototype.checkProperty = function(data) {
    if (this.node == null) throw 'this.node is null';
    if (this.defaultTool == undefined) throw 'this.defaultTool is undefined';
    if (! this.tools.has(this.defaultTool)) throw 'this.defaultTool does not exist';
    if (this.height == undefined || this.height < 1) throw 'Schemas.height is undefined or has bad value';
    if (this.width == undefined || this.width < 1) throw 'Schemas.width is undefined or has bad value';

    return true;
};

Schemas.prototype.initData = function(data) {
    this.node = document.getElementById(data.name);
    this.title = data.title;
    this.background = data.background;
    this.height = data.height;
    this.width = data.width;
    this.defaultTool = data.defaultTool;
};

Schemas.prototype.initTools = function() {
    this.tools.forEach(function(v,k) {
        v.name = k;

        // push schema reference into alls object tools
        v.schema = this;

        // Create button for select each tools
        // Note : button is added in dom by initHtml
        let button = document.createElement('button');
        v.button = button;
        button.classList.add('btn');
        // If is default tools button is
        if (k == this.defaultTool) {
            v.select(true);
        }
        else {v.select(false)};
        button.appendChild(document.createTextNode(v.label));
        const that = v; // Forward object tool reference into event
        button.addEventListener('click', function(e) {
            that.select(true);
        });

    }, this);
};

Schemas.prototype.initHtml = function() {

    /*
     * svg container
     */

    // create div container
    let svg_container = document.createElement('div');
    svg_container.setAttribute('class', 'row justify-content-md-center');
    this.node.appendChild(svg_container);

    // create the SVG node
    this.draw = SVG().addTo(svg_container).size(this.width, this.height);
    this.draw.node.setAttribute('id', this.node.id+'_svg');
    this.draw.node.setAttribute('style', 'border: solid 1px; background-color: #FFF; background-image: url('+this.background+');');

    /*
     * tool bar
     */

    // row for contain toolbar
    let toolbar_container = document.createElement('div');
    toolbar_container.setAttribute('class', 'row justify-content-between align-items-center mt-3');
    this.node.appendChild(toolbar_container);

    // col for contain selectot button for tools
    let tools_selector_col = document.createElement('div');
    tools_selector_col.setAttribute('class', 'col-auto');
    toolbar_container.appendChild(tools_selector_col);

    // btn group for selector btn tool
    let tools_selector_btns = document.createElement('div');
    tools_selector_btns.setAttribute('class', 'btn-group btn-group-toggle draw-btn-form');
    tools_selector_col.appendChild(tools_selector_btns);

    // create tools selector btn
    this.tools.forEach(function(v,k) {
        tools_selector_btns.appendChild(v.button);
    }, this);

    // tools property col
    let tools_property_col = document.createElement('div');
    tools_property_col.setAttribute('class', 'col-auto');
    toolbar_container.appendChild(tools_property_col);

};

Schemas.prototype.initEvents = function() {

    this.draw.on('mousedown', function(e) {
        this.selectedTool.mousedown(e);
    }, this);

    this.draw.on('mouseup', function(e) {
        this.selectedTool.mouseup(e);
    }, this);

    this.draw.on('mousemove', function(e) {
        this.selectedTool.mousemove(e);
    }, this);

};

/*
 * Schemas Action prototype
 */

function SchemasTool(property) {
    this.label = property.label;
    this.type = property.type;
    if (property.mousedown != undefined) {this.mousedown = property.mousedown};
    if (property.mouseup != undefined) {this.mouseup = property.mouseup};
    if (property.mousemove != undefined) {this.mousemove = property.mousemove};
    if (property.initAction != undefined) {this.initAction = property.initAction};
    if (property.doAction != undefined) {this.doAction = property.doAction};
    if (property.stopAction != undefined) {this.stopAction = property.stopAction};
};

SchemasTool.prototype.select = function (select)  {

    // do nothing is the tool is allready selected
    if (select && this.schema.selectedTool == this) return null;

    if (select) {
        if (this.schema.selectedTool != undefined) {this.schema.selectedTool.select(false)};
        this.schema.selectedTool = this;
        this.button.classList.remove('btn-outline-secondary');
        this.button.classList.add('btn-secondary');
    } else {
        this.stopAction();
        this.button.classList.add('btn-outline-secondary');
        this.button.classList.remove('btn-secondary');
    }
};

// Calcul la position relative du curseur de la souris
SchemasTool.prototype.calcCurPos = function(x, y) {
    let curpos = {
        'x': x - this.schema.x,
        'y': y - this.schema.y 
    };
    return curpos;
};

SchemasTool.prototype.mousedown = function() {
    return null;
};

SchemasTool.prototype.mouseup = function() {
    return null;
};

SchemasTool.prototype.mousemove = function() {
    return null;
};

SchemasTool.prototype.initAction = function() {
    return null;
};

SchemasTool.prototype.doAction = function() {
    return null;
};

SchemasTool.prototype.stopAction = function() {
    return null;
};

/* 
 * Shemas tool for draw rect
 */

Schemas.prototype.tools.set('rect', new SchemasTool({
	label: 'Rect',
    type: 'draw',

    mousedown: function(e) {
        this.initAction(e);
    },

    mouseup: function(e) {
        this.stopAction(e);
    },

    mousemove: function(e) {
        this.doAction(e);
    },

    initAction: function(e) {
        this.beginPos = this.calcCurPos(e.layerX, e.layerY);
        console.log(this.beginPos);
        this.drawing = true;
    },

    doAction: function(e) {
    },

    stopAction: function(e) {
        this.drawing = false;
    }
}));

/* 
 * Shemas tool for draw circle
 */

Schemas.prototype.tools.set('circle', new SchemasTool({
	label: 'Cercle',
    type: 'draw',
    mousedown: function(e) {
        this.initAction(e);
    },
    mouseup: function(e) {
        this.stopAction(e);
    },
    mousemove: function(e) {
        this.doAction(e);
    },
    initAction: function(e) {
    },
    doAction: function(e) {

    },
    stopAction: function(e) {
    }
}));

//vim:sw=4 sts=4 ts=4 expandtab
