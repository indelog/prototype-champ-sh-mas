# Prototype de champs schémas pour [Medshake EHR/ECD](https://www.logiciel-cabinet-medical.fr/)

Dépôt provisoire destiné à l'élaboration d'un prototype.

La discussion se trouve relative se trouve ici : <https://c-medshakeehr.fr/forum/viewtopic.php?f=1&t=7>

La démo du prototype ici : <https://c-medshakeehr.fr/proto-schemas/schemas.html>
